package me.projects.mobilebasiccnc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;

public class JawabanActivity extends AppCompatActivity {
    ViewStub jawabanView;
    int latihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jawaban);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        latihan = getIntent().getIntExtra("latihan",1);

        jawabanView = findViewById(R.id.jawabanView);

        showJawaban(latihan);
        getSupportActionBar().setTitle("Jawaban Latihan "+latihan);
    }

    public void showJawaban(int latihan){
        switch (latihan){
            case 1:
                jawabanView.setLayoutResource(R.layout.jawaban_latihan_1);
                jawabanView.inflate();
                jawabanView.setVisibility(View.VISIBLE);
                break;
            case 2:
                jawabanView.setLayoutResource(R.layout.jawaban_latihan_2);
                jawabanView.inflate();
                jawabanView.setVisibility(View.VISIBLE);
                break;
            case 3:
                jawabanView.setLayoutResource(R.layout.jawaban_latihan_3);
                jawabanView.inflate();
                jawabanView.setVisibility(View.VISIBLE);
                break;
            case 4:
                jawabanView.setLayoutResource(R.layout.jawaban_latihan_4);
                jawabanView.inflate();
                jawabanView.setVisibility(View.VISIBLE);
                break;
            case 5:
                jawabanView.setLayoutResource(R.layout.jawaban_latihan_5);
                jawabanView.inflate();
                jawabanView.setVisibility(View.VISIBLE);
                break;
            case 6:
                jawabanView.setLayoutResource(R.layout.jawaban_latihan_6);
                jawabanView.inflate();
                jawabanView.setVisibility(View.VISIBLE);
                break;
            default:
                jawabanView.setLayoutResource(R.layout.jawaban_latihan_1);
                jawabanView.inflate();
                jawabanView.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
