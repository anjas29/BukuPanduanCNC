package me.projects.mobilebasiccnc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class LatihanActivity extends AppCompatActivity {
    RelativeLayout latihan1Button, latihan2Button, latihan3Button, latihan4Button, latihan5Button, latihan6Button;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latihan);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        latihan1Button = findViewById(R.id.latihan1Button);
        latihan2Button = findViewById(R.id.latihan2Button);
        latihan3Button = findViewById(R.id.latihan3Button);
        latihan4Button = findViewById(R.id.latihan4Button);
        latihan5Button = findViewById(R.id.latihan5Button);
        latihan6Button = findViewById(R.id.latihan6Button);

        latihan1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLatihan(1);
            }
        });

        latihan2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLatihan(2);
            }
        });

        latihan3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLatihan(3);
            }
        });

        latihan4Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLatihan(4);
            }
        });

        latihan5Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLatihan(5);
            }
        });

        latihan6Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLatihan(6);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void goToLatihan(int latihan){
        Intent intent = new Intent(this, DetailLatihanActivity.class);
        intent.putExtra("latihan", latihan);
        startActivity(intent);
    }
}
