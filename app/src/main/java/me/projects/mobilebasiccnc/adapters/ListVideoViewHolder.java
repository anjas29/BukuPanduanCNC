package me.projects.mobilebasiccnc.adapters;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.projects.mobilebasiccnc.DetailVideoActivity;
import me.projects.mobilebasiccnc.R;

public class ListVideoViewHolder extends RecyclerView.ViewHolder{
    public TextView title, description;
    public ImageView image;

    public ListVideoViewHolder(final View v) {
        super(v);
        title = v.findViewById(R.id.titleView);
        description = v.findViewById(R.id.descriptionView);
        image = v.findViewById(R.id.imageView);

        title.setSelected(true);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(v.getContext(), DetailVideoActivity.class);
                String code = ListVideoAdapter.itemList.get(getAdapterPosition()).getUrl();
                intent.putExtra("code",code);
                v.getContext().startActivity(intent);
            }
        });
    }
}
