package me.projects.mobilebasiccnc.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.projects.mobilebasiccnc.DetailMateriActivity;
import me.projects.mobilebasiccnc.R;
import me.projects.mobilebasiccnc.objects.Bookmark;
import me.projects.mobilebasiccnc.objects.Video;


/**
 * Created by me on 10/11/16.
 */

public class ListBookmarkAdapter extends RecyclerView.Adapter<ListBookmarkViewHolder>{
    private Context context;
    public static ArrayList<Bookmark> itemList;

    DatabaseHelper db;

    public ListBookmarkAdapter(Context context, ArrayList<Bookmark> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
        db = new DatabaseHelper(context);
    }

    @Override
    public ListBookmarkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_bookmark, parent, false);
        ListBookmarkViewHolder view = new ListBookmarkViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListBookmarkViewHolder holder, final int position) {
        holder.name.setText(itemList.get(position).getName());
        holder.clickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailMateriActivity.class);
                int materi = Integer.parseInt(ListBookmarkAdapter.itemList.get(position).getMateri());
                intent.putExtra("materi", materi);
                v.getContext().startActivity(intent);
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder a = new AlertDialog.Builder(v.getContext())
                        .setTitle("Peringatan!")
                        .setMessage("Apakah anda yakin akan menghapus bookmark ini?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                db.deleteBookmark(ListBookmarkAdapter.itemList.get(position));
                                itemList.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                a.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
