package me.projects.mobilebasiccnc.adapters;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.projects.mobilebasiccnc.DetailMateriActivity;
import me.projects.mobilebasiccnc.R;

public class ListBookmarkViewHolder extends RecyclerView.ViewHolder{
    public TextView name;
    public RelativeLayout deleteButton;
    public RelativeLayout clickArea;

    public ListBookmarkViewHolder(final View v) {
        super(v);

        name = v.findViewById(R.id.nameView);
        deleteButton = v.findViewById(R.id.deleteButton);
        clickArea = v.findViewById(R.id.clickArea);


    }
}
