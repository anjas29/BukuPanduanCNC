package me.projects.mobilebasiccnc.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import me.projects.mobilebasiccnc.objects.Bookmark;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "mobilecnc";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create bookmarks table
        db.execSQL(Bookmark.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Bookmark.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public long insertBookmark(Bookmark bookmark) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Bookmark.COLUMN_NAME, bookmark.getName());
        values.put(Bookmark.COLUMN_MATERI, bookmark.getMateri());

        // insert row
        long id = db.insert(Bookmark.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Bookmark getBookmark(String name) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Bookmark.TABLE_NAME,
                new String[]{
                        Bookmark.COLUMN_ID,
                        Bookmark.COLUMN_NAME,
                        Bookmark.COLUMN_MATERI
                },
                Bookmark.COLUMN_NAME+ "=?",
                new String[]{String.valueOf(name)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare bookmark object
        Bookmark bookmark = new Bookmark(
                cursor.getInt(cursor.getColumnIndex(Bookmark.COLUMN_ID))+"",
                cursor.getString(cursor.getColumnIndex(Bookmark.COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(Bookmark.COLUMN_MATERI)));

        // close the db connection
        cursor.close();

        return bookmark;
    }

    public List<Bookmark> getAllBookmark() {
        List<Bookmark> bookmarks = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + Bookmark.TABLE_NAME + " ORDER BY " +
                Bookmark.COLUMN_ID + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                Bookmark bookmark = new Bookmark();
                bookmark.setId(cursor.getInt(cursor.getColumnIndex(Bookmark.COLUMN_ID))+"");
                bookmark.setName(cursor.getString(cursor.getColumnIndex(Bookmark.COLUMN_NAME)));
                bookmark.setMateri(cursor.getString(cursor.getColumnIndex(Bookmark.COLUMN_MATERI)));

                bookmarks.add(bookmark);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return bookmarks list
        return bookmarks;
    }

    public int getBookmarkCount() {
        String countQuery = "SELECT  * FROM " + Bookmark.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public void deleteBookmark(Bookmark bookmark) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Bookmark.TABLE_NAME, Bookmark.COLUMN_ID + " = ?",
                new String[]{String.valueOf(bookmark.getId())});
        db.close();
    }
}