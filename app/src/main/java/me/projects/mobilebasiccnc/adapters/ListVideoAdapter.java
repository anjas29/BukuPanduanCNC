package me.projects.mobilebasiccnc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.projects.mobilebasiccnc.R;
import me.projects.mobilebasiccnc.objects.Video;


/**
 * Created by me on 10/11/16.
 */

public class ListVideoAdapter extends RecyclerView.Adapter<ListVideoViewHolder>{
    private Context context;
    public static ArrayList<Video> itemList;

    public ListVideoAdapter(Context context, ArrayList<Video> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public ListVideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_video, parent, false);
        ListVideoViewHolder view = new ListVideoViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListVideoViewHolder holder, int position) {
        holder.title.setText(itemList.get(position).getName());
        //holder.description.setText(itemList.get(position).getDescription());
        holder.description.setText("");
        holder.image.setImageResource(itemList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
