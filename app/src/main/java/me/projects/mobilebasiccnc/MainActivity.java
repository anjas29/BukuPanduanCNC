package me.projects.mobilebasiccnc;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import me.projects.mobilebasiccnc.objects.Bookmark;

public class MainActivity extends AppCompatActivity {
    RelativeLayout kompetensiButton, materiButton, latihanButton, kuisButton, bantuanButton, videoButton, tentangButton, bookmarkButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        kompetensiButton = findViewById(R.id.kompetensiButton);
        materiButton = findViewById(R.id.materiButton);
        latihanButton = findViewById(R.id.latihanButton);
        kuisButton = findViewById(R.id.kuisButton);
        bantuanButton = findViewById(R.id.bantuanButton);
        videoButton = findViewById(R.id.videoButton);
        tentangButton = findViewById(R.id.tentangButton);
        bookmarkButton = findViewById(R.id.bookmarkButton);

        kompetensiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toKompetensiActivity();
            }
        });
        materiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toMateriActivity();
            }
        });
        latihanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toLatihanActivity();
            }
        });
        kuisButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toKuisActivity();
            }
        });
        videoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toVideoActivity();
            }
        });
        bantuanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toBantuanActivity();
            }
        });
        tentangButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toTentangActivity();
            }
        });
        bookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toBookmarkActivity();
            }
        });

    }

    public void toKompetensiActivity(){
        Intent intent = new Intent(this, KompetensiActivity.class);
        startActivity(intent);
    }

    public void toMateriActivity(){
        Intent intent = new Intent(this, MateriActivity.class);
        startActivity(intent);
    }

    public void toVideoActivity(){
        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
    }

    public void toLatihanActivity(){
        Intent intent = new Intent(this, LatihanActivity.class);
        startActivity(intent);
    }

    public void toKuisActivity(){
        Intent intent = new Intent(this, KuisActivity.class);
        startActivity(intent);
    }

    public void toBantuanActivity(){
        Intent intent = new Intent(this, BantuanActivity.class);
        startActivity(intent);
    }

    public void toTentangActivity(){
        Intent intent = new Intent(this, TentangActivity.class);
        startActivity(intent);
    }

    public void toBookmarkActivity(){
        Intent intent = new Intent(this, BookmarkActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder a = new AlertDialog.Builder(this)
                .setTitle("Keluar")
                .setMessage("Apakah anda yakin ingin keluar?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        a.show();
    }
}
