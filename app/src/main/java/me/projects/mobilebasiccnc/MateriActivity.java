package me.projects.mobilebasiccnc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MateriActivity extends AppCompatActivity {
    LinearLayout materi1Button, materi2Button, materi3Button, materi4Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        materi1Button = findViewById(R.id.materi1Button);
        materi2Button = findViewById(R.id.materi2Button);
        materi3Button = findViewById(R.id.materi3Button);
        materi4Button = findViewById(R.id.materi4Button);

        materi1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMateri(1);
            }
        });

        materi2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMateri(2);
            }
        });

        materi3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMateri(3);
            }
        });

        materi4Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMateri(4);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void goToMateri(int materi){
        Intent intent = new Intent(this, DetailMateriActivity.class);
        intent.putExtra("materi", materi);
        startActivity(intent);
    }
}