package me.projects.mobilebasiccnc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HasilKuisActivity extends AppCompatActivity {
    TextView nameView, scoreView, descriptionView;
    String nama;
    int nilai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_kuis);

        nama = getIntent().getStringExtra("nama");
        nilai = getIntent().getIntExtra("nilai",0);

        nameView = findViewById(R.id.nameView);
        scoreView = findViewById(R.id.scoreView);
        descriptionView = findViewById(R.id.descriptionView);

        nameView.setText("Hallo "+nama);
        scoreView.setText("Nilaimu adalah "+nilai+" dari 100");
        if(nilai>59){
            descriptionView.setText("Pertahankan terus nilaimu dengan belajar secara rutin");
        }else{
            descriptionView.setText("Wah anda harus rajin belajar lagi");
        }
    }
}
