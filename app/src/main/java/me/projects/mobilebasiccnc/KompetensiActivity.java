package me.projects.mobilebasiccnc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class KompetensiActivity extends AppCompatActivity {
    String kompetensiDasar [];
    String kompetensiInti [];
    ImageView beforeButton, afterButton;
    TextView kompetensiIntiView, kompetensiDasarView;
    int current, arrayLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kompetensi);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        kompetensiDasar = getResources().getStringArray(R.array.kompetensi_dasar);
        kompetensiInti = getResources().getStringArray(R.array.komptensi_inti);

        current = 0;
        arrayLength = kompetensiDasar.length;

        beforeButton = findViewById(R.id.beforeButton);
        afterButton = findViewById(R.id.afterButton);
        kompetensiIntiView = findViewById(R.id.kompetensi_inti_view);
        kompetensiDasarView = findViewById(R.id.kompetensi_dasar_view);

        kompetensiIntiView.setText(kompetensiInti[current]);
        kompetensiDasarView.setText(kompetensiDasar[current]);

        beforeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBefore();
            }
        });

        afterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNext();
            }
        });
    }

    public void setBefore(){
        current--;
        if(current < 0){
            current = arrayLength-1;
        }else if(current >= arrayLength){
            current = 0;
        }

        kompetensiIntiView.setText(kompetensiInti[current]);
        kompetensiDasarView.setText(kompetensiDasar[current]);
    }

    public void setNext(){
        current++;
        if(current < 0){
            current = arrayLength-1;
        }else if(current >= arrayLength){
            current = 0;
        }

        kompetensiIntiView.setText(kompetensiInti[current]);
        kompetensiDasarView.setText(kompetensiDasar[current]);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
