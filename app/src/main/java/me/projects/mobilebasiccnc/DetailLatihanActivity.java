package me.projects.mobilebasiccnc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.github.barteksc.pdfviewer.PDFView;
import com.jsibbold.zoomage.ZoomageView;

public class DetailLatihanActivity extends AppCompatActivity {
    int latihan;
    RelativeLayout jawabanButton;
    ZoomageView latihanView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_latihan);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        latihan = getIntent().getIntExtra("latihan",1);
        jawabanButton = findViewById(R.id.jawabanButton);
        latihanView = findViewById(R.id.latihanView);

        getSupportActionBar().setTitle("Soal Latihan "+latihan);

        jawabanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailLatihanActivity.this, JawabanActivity.class);
                intent.putExtra("latihan", latihan);
                startActivity(intent);
            }
        });
        
        showSoal(latihan);
    }
    
    public void showSoal(int latihan){
        switch (latihan){
            case 1:
                latihanView.setImageResource(R.drawable.latihan_1);
                break;
            case 2:
                latihanView.setImageResource(R.drawable.latihan_2);
                break;
            case 3:
                latihanView.setImageResource(R.drawable.latihan_3);
                break;
            case 4:
                latihanView.setImageResource(R.drawable.latihan_4);
                break;
            case 5:
                latihanView.setImageResource(R.drawable.latihan_5);
                break;
            case 6:
                latihanView.setImageResource(R.drawable.latihan_6);
                break;
            default:
                latihanView.setImageResource(R.drawable.latihan_1);
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
