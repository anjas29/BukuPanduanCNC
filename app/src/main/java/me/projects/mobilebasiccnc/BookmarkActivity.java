package me.projects.mobilebasiccnc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import me.projects.mobilebasiccnc.adapters.DatabaseHelper;
import me.projects.mobilebasiccnc.adapters.ListBookmarkAdapter;
import me.projects.mobilebasiccnc.adapters.ListVideoAdapter;
import me.projects.mobilebasiccnc.objects.Bookmark;
import me.projects.mobilebasiccnc.objects.Video;

public class BookmarkActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static ArrayList<Bookmark> itemList;
    private LinearLayoutManager layoutManager;
    DatabaseHelper db;
    TextView messageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        db = new DatabaseHelper(this);

        recyclerView = findViewById(R.id.list_view);
        messageView = findViewById(R.id.messageView);
        layoutManager= new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        getData();
    }

    public void getData(){
        itemList = new ArrayList<>();

        for(Bookmark b:db.getAllBookmark()){
            itemList.add(b);
        }

        if(itemList.isEmpty()){
            messageView.setVisibility(View.VISIBLE);
        }else {
            ListBookmarkAdapter adapter = new ListBookmarkAdapter(this, itemList);
            recyclerView.setAdapter(adapter);
            recyclerView.setHasFixedSize(true);

            recyclerView.setLayoutManager(layoutManager);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
