package me.projects.mobilebasiccnc;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jsibbold.zoomage.ZoomageView;

import java.util.ArrayList;
import java.util.Random;

import me.projects.mobilebasiccnc.objects.Soal;

public class DetailKuisActivity extends AppCompatActivity {
    String nama;
    RelativeLayout aButton, bButton, cButton, dButton, eButton;
    ArrayList<Soal> soal;
    TextView soalView, aView, bView, cView, dView, eView;
    int current;
    int benar=0;
    String [] soalArray, aArray, bArray, cArray, dArray, eArray, jawaban;
    String currentJawaban = null;
    ImageView nextButton;
    EditText namaView;
    TextView soalNumberView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kuis);

        current = 0;
        currentJawaban = "";

        nama = getIntent().getExtras().getString("name");

        aButton = findViewById(R.id.aButton);
        bButton = findViewById(R.id.bButton);
        cButton = findViewById(R.id.cButton);
        dButton = findViewById(R.id.dButton);
        eButton = findViewById(R.id.eButton);
        soalView = findViewById(R.id.soalView);
        aView = findViewById(R.id.aOption);
        bView = findViewById(R.id.bOption);
        cView = findViewById(R.id.cOption);
        dView = findViewById(R.id.dOption);
        eView = findViewById(R.id.eOption);
        nextButton = findViewById(R.id.nextButton);
        namaView = findViewById(R.id.namaView);
        soalNumberView = findViewById(R.id.soalNumber);

        seedSoal();

        aButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentJawaban = "a";
                setBackgroundButton(currentJawaban, false);
            }
        });

        bButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentJawaban = "b";
                setBackgroundButton(currentJawaban, false);
            }
        });

        cButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentJawaban = "c";
                setBackgroundButton(currentJawaban, false);
            }
        });

        dButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentJawaban = "d";
                setBackgroundButton(currentJawaban, false);
            }
        });

        eButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentJawaban = "e";
                setBackgroundButton(currentJawaban, false);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextSoal(currentJawaban);
            }
        });

        setSoal(current);
        namaView.setText(nama);
    }

    public void setBackgroundButton(String jawaban, boolean baru){
        aButton.setBackgroundResource(R.drawable.dark_green_round_shape);
        bButton.setBackgroundResource(R.drawable.dark_green_round_shape);
        cButton.setBackgroundResource(R.drawable.dark_green_round_shape);
        dButton.setBackgroundResource(R.drawable.dark_green_round_shape);
        eButton.setBackgroundResource(R.drawable.dark_green_round_shape);

        if(!baru){
            switch (jawaban){
                case "a":
                    aButton.setBackgroundResource(R.drawable.dark_blue_round_shape);
                    break;
                case "b":
                    bButton.setBackgroundResource(R.drawable.dark_blue_round_shape);
                    break;
                case "c":
                    cButton.setBackgroundResource(R.drawable.dark_blue_round_shape);
                    break;
                case "d":
                    dButton.setBackgroundResource(R.drawable.dark_blue_round_shape);
                    break;
                case "e":
                    eButton.setBackgroundResource(R.drawable.dark_blue_round_shape);
                    break;
            }
        }
    }


    public void nextSoal(String jawaban){
        if(current<soal.size()-1){
            if(jawaban.equals(soal.get(current).getJawaban())){
                benar++;
            }
            current++;
            setSoal(current);
            currentJawaban = "";
            setBackgroundButton(currentJawaban, true);
        }else{
            int nilai = benar*100/soal.size();
            Intent intent = new Intent(this, HasilKuisActivity.class);
            intent.putExtra("nama", namaView.getText().toString());
            intent.putExtra("nilai",nilai);
            startActivity(intent);
            finish();
        }
    }

    public void setSoal(int index){
        soalView.setText(soal.get(index).getSoal());
        aView.setText(soal.get(index).getOptionA());
        bView.setText(soal.get(index).getOptionB());
        cView.setText(soal.get(index).getOptionC());
        dView.setText(soal.get(index).getOptionD());
        eView.setText(soal.get(index).getOptionE());

        soalNumberView.setText(index+1+"/"+soal.size());
    }

    public void seedSoal(){
        soal = new ArrayList<>();
        soalArray = getResources().getStringArray(R.array.soal);
        aArray = getResources().getStringArray(R.array.optionA);
        bArray = getResources().getStringArray(R.array.optionB);
        cArray = getResources().getStringArray(R.array.optionC);
        dArray = getResources().getStringArray(R.array.optionD);
        eArray = getResources().getStringArray(R.array.optionE);
        jawaban= getResources().getStringArray(R.array.jawaban);

        for(int i=0;i<soalArray.length;i++){
            soal.add(new Soal(soalArray[i]));
            soal.get(i).setOptionA(aArray[i]);
            soal.get(i).setOptionB(bArray[i]);
            soal.get(i).setOptionC(cArray[i]);
            soal.get(i).setOptionD(dArray[i]);
            soal.get(i).setOptionE(eArray[i]);
            soal.get(i).setJawaban(jawaban[i]);
        }

        shuffleSoal();
    }

    public void shuffleSoal(){
        Random r = new Random();
        int index;
        Soal temp;
        for(int i=0;i<soal.size();i++){
            index = r.nextInt(soal.size());
            temp = soal.get(i);
            soal.set(i,soal.get(index));
            soal.set(index,temp);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder a = new AlertDialog.Builder(this)
                .setTitle("Keluar")
                .setMessage("Apakah anda yakin ingin mengakhiri kuis?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        a.show();
    }
}
