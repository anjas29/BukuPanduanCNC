package me.projects.mobilebasiccnc.objects;

public class Soal {
    public String soal;
    public String optionA;
    public String optionB;
    public String optionC;
    public String optionD;
    public String optionE;
    public String jawaban;

    public Soal(String soal) {
        this.soal = soal;
        this.optionA = null;
        this.optionB = null;
        this.optionC = null;
        this.optionD = null;
        this.optionE = null;

        this.jawaban = null;
    }

    public String getSoal() {
        return soal;
    }

    public void setSoal(String soal) {
        this.soal = soal;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getOptionE() {
        return optionE;
    }

    public void setOptionE(String optionE) {
        this.optionE = optionE;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }
}
