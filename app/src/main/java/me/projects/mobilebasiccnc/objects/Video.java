package me.projects.mobilebasiccnc.objects;

public class Video {
    String name;
    int image;
    String url;
    String description;

    public Video(String name, int image, String url) {
        this.name = name;
        this.image = image;
        this.url = url;
        this.description = null;
    }

    public Video(String name, int image, String url, String description) {
        this.name = name;
        this.image = image;
        this.url = url;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
