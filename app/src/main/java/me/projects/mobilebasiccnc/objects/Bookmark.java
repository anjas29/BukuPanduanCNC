package me.projects.mobilebasiccnc.objects;

public class Bookmark {
    public static final String TABLE_NAME = "device";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_MATERI = "materi";

    private String id;
    private String name;
    private String materi;

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " VARCHAR(225),"
                    + COLUMN_MATERI + " INTEGER(225)"
                    + ")";

    public Bookmark() {
    }

    public Bookmark(String name, String materi) {
        this.name = name;
        this.materi = materi;
    }

    public Bookmark(String id, String name, String materi) {
        this.id = id;
        this.name = name;
        this.materi = materi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMateri() {
        return materi;
    }

    public void setMateri(String materi) {
        this.materi = materi;
    }
}
