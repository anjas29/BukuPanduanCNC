package me.projects.mobilebasiccnc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

public class LoadingScreenActivity extends AppCompatActivity{
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_screen);
        progressBar = findViewById(R.id.progressBar);

        new Thread(new Runnable() {
            public void run() {
                doLoading();
                toMainActivity();
                finish();
            }
        }).start();
    }

    public void doLoading(){
        for (int progress=0; progress<100; progress+=4) {
            try {
                Thread.sleep(60);
                progressBar.setProgress(progress);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void toMainActivity(){
        Intent intent = new Intent(this,OpeningActivity.class);
        startActivity(intent);
    }
}
