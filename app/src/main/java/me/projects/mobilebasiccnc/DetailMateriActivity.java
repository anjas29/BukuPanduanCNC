package me.projects.mobilebasiccnc;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;

import java.util.ArrayList;
import java.util.List;

import me.projects.mobilebasiccnc.adapters.DatabaseHelper;
import me.projects.mobilebasiccnc.objects.Bookmark;

public class DetailMateriActivity extends AppCompatActivity {
    int materi = 1;
    PDFView pdfView;
    FloatingActionButton bookmarkButton;
    DatabaseHelper db;
    String docName;
    TextView tujuanView;
    String tujuanArray [];
    String judulArray [];
    String tujuan;
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_materi);


        db = new DatabaseHelper(this);
        materi = getIntent().getIntExtra("materi",1);
        bookmarkButton = findViewById(R.id.bookmarkButton);
        bookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBookmark();
            }
        });

        webview = findViewById(R.id.webView);
        showMateri(materi);
    }

    public void showMateri(int materi){
        switch (materi){
            case 1:
                docName = "materi_1.html";
                break;
            case 2:
                docName = "materi_2.html";
                break;
            case 3:
                docName = "materi_3.html";
                break;
            case 4:
                docName = "materi_4.html";
                break;
            default:
                docName = "materi_1.html";
                break;
        }

        webview.loadUrl("file:///android_asset/"+docName);
        webview.getSettings().setJavaScriptEnabled(true);
    }

    public void addBookmark(){
        boolean saved = false;
        List<Bookmark> bookmarks = db.getAllBookmark();
        for(Bookmark b:bookmarks){
            if(b.getMateri().equals(materi+"")){
                saved = true;
                break;
            }
        }
        if(!saved){
            db.insertBookmark(new Bookmark("MATERI "+materi,materi+""));
            Toast.makeText(this,"Bookmark berhasil ditambahkan", Toast.LENGTH_SHORT).show();
        }else{
            AlertDialog.Builder a = new AlertDialog.Builder(this)
                    .setTitle("Peringatan!")
                    .setMessage("Bookmark sudah ada")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            a.show();
        }        
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
