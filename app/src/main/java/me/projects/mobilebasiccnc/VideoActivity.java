package me.projects.mobilebasiccnc;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import me.projects.mobilebasiccnc.adapters.ListVideoAdapter;
import me.projects.mobilebasiccnc.objects.Video;

public class VideoActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static ArrayList<Video> itemList;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = findViewById(R.id.list_view);
        layoutManager= new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        getData();
    }

    public void getData(){
        itemList = new ArrayList<>();

        itemList.add(new Video("Basic CNC Programming  CNC Programming for beginners  CNC Programming", R.drawable.video1, "ZgWYoFWTKJc","Pengenalan lingkungan kerja CNC"));
        itemList.add(new Video("CNC Hand Programming Final", R.drawable.video2, "Sid-P8D7U6U","Pengenalan lingkungan kerja CNC bagian 2"));
        itemList.add(new Video("Cutting Ability WIth CNC Lathe Seco and Ingersool", R.drawable.video3, "0nMSzQeSgJo","Pengenalan lingkungan kerja CNC"));
        itemList.add(new Video("CNC Lathe Hand Programming", R.drawable.video4, "L7LFz7ud71Q","Pengenalan lingkungan kerja CNC bagian 2"));
        itemList.add(new Video("CNC Programming and CNC Lathe Process", R.drawable.video5, "jsw32mpJfoM","Pengenalan lingkungan kerja CNC"));
        itemList.add(new Video("CNC Lathe Setup Part 1", R.drawable.video6, "fTPluUa6be0","Pengenalan lingkungan kerja CNC bagian 2"));
        itemList.add(new Video("CNC Lathe Setup Part 2", R.drawable.video7, "ph79hia_LTw","Pengenalan lingkungan kerja CNC bagian 2"));
        itemList.add(new Video("CNC Machine Programming in Hindi", R.drawable.video8, "95HZb8n7mVI","Pengenalan lingkungan kerja CNC bagian 2"));
        itemList.add(new Video("World Largest Lathe Machine Operation Video", R.drawable.video9, "QaZ-WWxRQBw","Pengenalan lingkungan kerja CNC bagian 2"));

        ListVideoAdapter adapter = new ListVideoAdapter(getBaseContext(), itemList);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
